package org.teamcascade.tcsuite.user;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;


import org.spout.api.chat.ChatArguments;
import org.spout.api.entity.Player;
import org.spout.api.event.EventHandler;
import org.spout.api.event.Listener;
import org.spout.api.event.Order;
import org.spout.api.event.player.PlayerChatEvent;
import org.teamcascade.tcsuite.AOSuite;
import org.teamcascade.tcsuite.chat.ChannelView;
import org.teamcascade.tcsuite.chat.viewPort;


public class User implements Listener  {
	
	// Fields
	private String userName ;					// Name of user on Spout Server
	private Player userEntity;					// Player class entity on Spout Server
	private File userFolder;					// Folder where user configuration files are located
	private Calendar userLastOnline;			// Date and time the user was last online
	private String userLastChannel;				// Last used chat channel
	private String userLastPM;					// User name of the last PM received 
	
	private ArrayList<String> userStatus; 		// User status array
	private ArrayList<viewPort> userViewPorts;	// Contains a list of all chat windows defined by the user


	
	// Class Constructors
	public User(String pUserName){
			userName = pUserName;
	}

	// Getters and Setters
	public String getUserName() {
		return userName;
	}
	
	public void setUserEntity(Player pPlayer) {
		userEntity = pPlayer;
	}
	public Player getUserEntity() {
		return userEntity ;
	}
	
	public void setUserLastOnline(Calendar pCalendar) {
		userLastOnline = pCalendar;
	}
	public Calendar getUserLastOnline() {
		return userLastOnline ;
	}
	
	public File getUserFolder() {
		return userFolder;
	}
	public void setUserFolder(File pFolder) {
		userFolder = pFolder;
	}
	
	public String getUserLastPM() {
		return userLastPM;
	}

	public void setUserLastPM(String pUserName) {
		userLastPM = pUserName;
	}


	public String getUserLastChannel() {
		return userLastChannel;
	}

	public void setUserLastChannel(String pChannelName) {
		userLastChannel = pChannelName;
	}

   // Event Handlers

	/**
	 * Handles the player chat event and send message to user if he is listening
	 * @param event
	 */
		@EventHandler(order = Order.DEFAULT_IGNORE_CANCELLED)

		public void onPlayerChatEvent(PlayerChatEvent event) {
		  ChatArguments msgMessage = event.getMessage();
		  String msgUserName = event.getPlayer().getName();
		  String msgChannelName = AOSuite.getUsersManager().getUser(msgUserName).getUserLastChannel();
		  if (msgChannelName == null) {
			  // TODO Default channel name should be defined as part of the plug-in configuration file
			  msgChannelName = "general";
		  }
		  // check if the user is listening to a specific channel in each ViewPort (chat window)
		    viewPort port;
			ChatArguments msg = new ChatArguments("["+msgChannelName+"] <"+msgUserName+"> "+msgMessage.asString());
			Player userEntity = AOSuite.getUsersManager().getUser(userName).getUserEntity();
			for (int i = 0;i < userViewPorts.size();i++) {
				port = userViewPorts.get(i);
				if (port.getChannelView(msgChannelName) != null) {
					userEntity.sendMessage(msg);

				}

			}

		  event.setCancelled(true);

		  

		}

		

		/**
		 * Register the event listener in the spout engine event manager
		 */

		

		public void registerListener() {

			AOSuite.getInstance().getEngine().getEventManager().registerEvents(this, AOSuite.getInstance());

		}



	// Class Methods //
	public boolean addStatus(String pStatus){
		return userStatus.add(pStatus);
		
		}
	
	public boolean removeStatus(String pStatus) {
		return userStatus.remove(pStatus);
	}
	
	public boolean hasStatus(String pStatus) {
		return userStatus.contains(pStatus);
	}
	
	public void removeViewPort(viewPort pViewPort) {
	   for (int i = 0; i < userViewPorts.size(); i++) {
		   if (userViewPorts.get(i) == pViewPort) {
			   userViewPorts.remove(i);
			   return;
		   }
	   }
	}
		
	public void addViewPort(viewPort pViewPort) {
		userViewPorts.add(pViewPort);
	}
		
	public void addChannelView(String channelName, ChannelView channelView) {
		// Add the view to all viewPorts until client-side phase
		for (int i = 0;i < userViewPorts.size();i++) {
			 userViewPorts.get(i).setChannelView(channelName, channelView);
		}
		
	}
	
	public void removeChannelView(String channelName) {
		// Remove the view from all viewPorts until client-side phase
		for (int i = 0;i < userViewPorts.size();i++) {
			 userViewPorts.get(i).removeChannelView(channelName);
		}
		
	}

	public void readData() {
		// TODO Auto-generated method stub
		
	}

	public void writeData() {
		// TODO Auto-generated method stub
		
	}

	
	
 }
