package org.teamcascade.tcsuite.user;

import java.util.HashMap;



public class UsersManager {
	
	// Holds the currently connected users and their preferences
	HashMap<String,User> Users ;
	
	public UsersManager(){
		Users = new HashMap<String,User>();
	}
	
	public void addUser (User pUser) {
		Users.put(pUser.getUserName(), pUser);

	}
	
	public void removeUser(User pUser) {
		Users.remove(pUser.getUserName());
	}

	public User getUser(String pUserName) {
		
		return Users.get(pUserName);
	}
	
	public boolean Exists(String pChannelName) {
		return Users.containsKey(pChannelName);
	}



}
