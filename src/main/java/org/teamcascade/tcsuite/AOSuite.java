package org.teamcascade.tcsuite;


import java.io.File;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;




import org.spout.api.Engine;
import org.spout.api.Spout;
import org.spout.api.exception.ConfigurationException;
import org.spout.api.plugin.CommonPlugin;
import org.spout.api.plugin.PluginManager;
import org.spout.api.util.config.ConfigurationHolder;
import org.teamcascade.tcsuite.chat.ChatChannel;
import org.teamcascade.tcsuite.chat.ChatChannelManager;
import org.teamcascade.tcsuite.chat.ChatMessageManager;
import org.teamcascade.tcsuite.configuration.ChannelConfiguration;
import org.teamcascade.tcsuite.configuration.PluginConfiguration;
import org.teamcascade.tcsuite.user.UsersManager;


public class AOSuite extends CommonPlugin {
	
	/**
	 * Used to identify this plug-in.
	 * The pluginID should be used to indicate the origin of system messages
	 */
	public static String pluginID ;
	
	/**
	 * Used to identify the type of data handling
	 * Currently only SQLData is available (possible plans for mexDB or Simplesave later on)
	 */
	public static String dataType ;
	
	/**
	 * References the instance of the class.
	 * The AOInstance can be used to access the plugin static members
	 */
	public static AOSuite instance;
	
	

	/**
	 * Used to signify that debug-mode is on.
	 * This default's to the engine debug-mode but can be overwritten at the plugin level
	 */
	public static boolean debugMode;

	/**
	 * Used to signify that method profiling is on.
	 * This default's to false but can be set in the configuration
	 */
	public static boolean profileMode;

	/**
	 * Used to hold this plug-in general configuration.
	 */
	public static PluginConfiguration pluginConfig ;
	
	/**
	 * Used to hold the plugin default chat channels.
	 */
	public static ChannelConfiguration channelConfig ;

	/**
	 *  Users directory
	 */
	public static File usersFolder;

	// Game engine 
	public Engine engine;


	/**
	 * Handles chat channel management for the plug-in.
	 */
	public static ChatChannelManager channelsManager = new ChatChannelManager();
	
	/**
	 * Handles users management for the plug-in.
	 */

    public static UsersManager usersManager = new UsersManager();
    
    /**
     * True when the chat functionality is enabled in configuration
     */
    public static boolean chatEnabled;
 
    /**
     * True when the friends list functionality is enabled in configuration
     */
    public static boolean friendsEnabled;
	
    /**
     * True when the ignore list functionality is enabled in configuration
     */
    public static boolean ignoreEnabled;
	
    /**
     * True when the grouping functionality is enabled in configuration
     */
    public static boolean groupsEnabled;
	
    /**
     * True when the clan management functionality is enabled in configuration
     */
    public static boolean clansEnabled;
	
    /**
     * True when the mail functionality is enabled in configuration
     */
    public static boolean mailEnabled;
    
    /**
     * True when the news functionality is enabled in configuration
     */
    public static boolean newsEnabled;
    
    /**
     * True when the polling functionality is enabled in configuration
     */
    public static boolean pollsEnabled;
    
    /**
     * True when the in-game wiki functionality is enabled in configuration
     */
    public static boolean wikiEnabled;
	
    /**
     * True when the in-game help functionality is enabled in configuration
     */
    public static boolean helpEnabled;
	
    /**
     * True when the cross-server functionality is enabled in configuration
     */
    public static boolean crossEnabled;
	    
    /**
     * True when the voice functionality is enabled in configuration
     */
    public static boolean voiceEnabled;

	
	// Setters and Getters
	
	public static boolean isVoiceEnabled() {
		return voiceEnabled;
	}

	public static void setVoiceEnabled(boolean voiceEnabled) {
		AOSuite.voiceEnabled = voiceEnabled;
	}

	public static String getPluginID() {
		return pluginID;
	}
	
	public static CommonPlugin getInstance() {
		return instance;
	}


	public static String getDataType() {
		return dataType;
	}

	public static UsersManager getUsersManager() {
		return usersManager;
	}

	public static ChatChannelManager getChannelsManager() {
		return channelsManager;
	}

	public static ChatMessageManager getChatMessageManager() {
		// TODO Auto-generated method stub
		return null;
	}

	public static File getUsersFolder() {
	
		return usersFolder;
	}

	
	public static boolean isChatEnabled() {
		return chatEnabled;
	}

	public static void setChatEnabled(boolean chatEnabled) {
		AOSuite.chatEnabled = chatEnabled;
	}


	public static boolean isFriendsEnabled() {
		return friendsEnabled;
	}

	public static void setFriendsEnabled(boolean friendsEnabled) {
		AOSuite.friendsEnabled = friendsEnabled;
	}

	public static boolean isHelpEnabled() {
		return helpEnabled;
	}

	public static void setHelpEnabled(boolean helpEnabled) {
		AOSuite.helpEnabled = helpEnabled;
	}

	public static boolean isCrossEnabled() {
		return crossEnabled;
	}

	public static void setCrossEnabled(boolean crossEnabled) {
		AOSuite.crossEnabled = crossEnabled;
	}
	
	
	public static boolean isNewsEnabled() {
		return newsEnabled;
	}

	public static void setNewsEnabled(boolean newsEnabled) {
		AOSuite.newsEnabled = newsEnabled;
	}

	public static boolean isPollsEnabled() {
		return pollsEnabled;
	}

	public static void setPollsEnabled(boolean pollsEnabled) {
		AOSuite.pollsEnabled = pollsEnabled;
	}

	public static boolean isClansEnabled() {
		return clansEnabled;
	}

	public static void setClansEnabled(boolean clansEnabled) {
		AOSuite.clansEnabled = clansEnabled;
	}

	public static boolean isGroupsEnabled() {
		return groupsEnabled;
	}

	public static boolean isWikiEnabled() {
		return wikiEnabled;
	}

	public static void setWikiEnabled(boolean wikiEnabled) {
		AOSuite.wikiEnabled = wikiEnabled;
	}

	public static void setGroupsEnabled(boolean groupsEnabled) {
		AOSuite.groupsEnabled = groupsEnabled;
	}

	public static boolean isMailEnabled() {
		return mailEnabled;
	}

	public static void setMailEnabled(boolean mailEnabled) {
		AOSuite.mailEnabled = mailEnabled;
	}

	public static boolean isIgnoreEnabled() {
		return ignoreEnabled;
	}

	public static void setIgnoreEnabled(boolean ignoreEnabled) {
		AOSuite.ignoreEnabled = ignoreEnabled;
	}

	/**
	 * @param AOInstance the reference to the plug-in
	 */
	public static void setInstance(AOSuite pInstance) {
		instance = pInstance;
	}
	public static void setDataType(String pDataType) {
		dataType = pDataType;
	}
	
	public static void setUsersFolder(File pFolder) {
		usersFolder = pFolder;
	}


	// Methods Overrides
	@Override
	public void onEnable() {
		engine = this.getEngine();
		setInstance(this);
		setDataType("SQL");
		pluginID = this.getName();
		debugMode = engine.debugMode();
		profileMode = false ;

		// Initialize users folder 
		setUsersFolder(new File(this.getDataFolder().getPath()+System.getProperty("file.separator")+"users"));

		// Load global Configuration
		pluginConfig = new PluginConfiguration(this.getDataFolder());
		try {
			pluginConfig.load();
			AOSuite.setChatEnabled((Boolean) pluginConfig.getNode("module", "chat").getValue());
			getLogger().info(" Enable chat functionality: "+AOSuite.isChatEnabled());
			AOSuite.setFriendsEnabled((Boolean) pluginConfig.getNode("module", "friends").getValue());
			getLogger().info(" Enable friends list functionality: "+AOSuite.isFriendsEnabled());
			AOSuite.setIgnoreEnabled((Boolean) pluginConfig.getNode("module", "ignore").getValue());
			getLogger().info(" Enable ignored list functionality: "+AOSuite.isIgnoreEnabled());
			AOSuite.setMailEnabled((Boolean) pluginConfig.getNode("module", "mail").getValue());
			getLogger().info(" Enable in-game mail functionality: "+AOSuite.isMailEnabled());
			AOSuite.setGroupsEnabled((Boolean) pluginConfig.getNode("module", "group").getValue());
			getLogger().info(" Enable grouping functionality: "+AOSuite.isGroupsEnabled());
			AOSuite.setClansEnabled((Boolean) pluginConfig.getNode("module", "clan").getValue());
			getLogger().info(" Enable clans functionality: "+AOSuite.isClansEnabled());
			AOSuite.setHelpEnabled((Boolean) pluginConfig.getNode("module", "help").getValue());
			getLogger().info(" Enable help functionality: "+AOSuite.isHelpEnabled());
			AOSuite.setCrossEnabled((Boolean) pluginConfig.getNode("module", "cross-server").getValue());
			getLogger().info(" Enable cross-server functionality: "+AOSuite.isCrossEnabled());
			AOSuite.setNewsEnabled((Boolean) pluginConfig.getNode("module", "news").getValue());
			getLogger().info(" Enable news functionality: "+AOSuite.isNewsEnabled());
			AOSuite.setPollsEnabled((Boolean) pluginConfig.getNode("module", "poll").getValue());
			getLogger().info(" Enable polls functionality: "+AOSuite.isPollsEnabled());
			AOSuite.setWikiEnabled((Boolean) pluginConfig.getNode("module", "wiki").getValue());
			getLogger().info(" Enable wiki functionality: "+AOSuite.isWikiEnabled());
			AOSuite.setVoiceEnabled((Boolean) pluginConfig.getNode("module", "voice").getValue());
			getLogger().info(" Enable voice functionality: "+AOSuite.isVoiceEnabled());

			// load module specific configuration when enabled
			if (isChatEnabled()) {
				channelConfig = new ChannelConfiguration(this.getDataFolder());
				ChatChannel channel;
				String channelName;
				// create default channels
				try {
					channelConfig.load();
					LinkedHashMap<?,?> nodes = (LinkedHashMap<?, ?>) channelConfig.getValues();
					for (Entry<?, ?> entry : nodes.entrySet()) {
					    channelName = (String) entry.getKey();
				    	channel = new ChatChannel(channelName);
				    	if (channel != null) {
				    	    channelsManager.addChatChannel(channel);
					    	this.getLogger().info("Channel "+channel.getChName()+" created.");

				    	} else {
				    		this.getLogger().info("Encountered a NULL channel, verify channels configuration file");
				    	}
			
					    
					}				
				} catch (ConfigurationException e) {
				getLogger().log(Level.WARNING, "Error loading "+ AOSuite.getPluginID()+" channels configuration: ", e);
			}
			}

			} catch (ConfigurationException e) {
			getLogger().log(Level.WARNING, "Error loading "+ getPluginID()+" configuration: ", e);
		}

		
		// Log status
		getLogger().info(" v" + getDescription().getVersion() + " has been enabled.");
	
	}
	
	@Override
	public void onLoad() {
		// Save global chat configuration 

		// Save default channels
		
		// Release instance from memory
	
		// Log Status
		getLogger().info(" v" + getDescription().getVersion() + " has been loaded.");
		
	}

	


	@Override
	public void onDisable() {
		// Save global chat configuration 

		// Save default channels
		
		// Release instance from memory
	
		// Log Status
		getLogger().info(" v" + getDescription().getVersion() + " has been disabled.");
		
	}



}
