package org.teamcascade.tcsuite.configuration;

import java.io.File;

import org.spout.api.exception.ConfigurationException;
import org.spout.api.util.config.ConfigurationHolder;
import org.spout.api.util.config.ConfigurationHolderConfiguration;
import org.spout.api.util.config.yaml.YamlConfiguration;

public class PluginConfiguration  extends ConfigurationHolderConfiguration {

	// Modules
	public static final ConfigurationHolder CHATENABLED = new ConfigurationHolder(true, "module", "chat");					// Enable chat
	public static final ConfigurationHolder FRIENDSENABLED = new ConfigurationHolder(true, "module", "friends");			// Enable friends list
	public static final ConfigurationHolder IGNOREENABLED = new ConfigurationHolder(true, "module", "ignore");				// Enable ignore list
	public static final ConfigurationHolder MAILENABLED = new ConfigurationHolder(true, "module", "mail");					// Enable in-game mail
	public static final ConfigurationHolder GROUPENABLED = new ConfigurationHolder(true, "module", "group");				// Enable grouping
	public static final ConfigurationHolder HELPENABLED = new ConfigurationHolder(true, "module", "help");					// Enable help book
	public static final ConfigurationHolder CLANENABLED = new ConfigurationHolder(true, "module", "clan");					// Enable clan management
	public static final ConfigurationHolder CROSSENABLED = new ConfigurationHolder(true, "module", "cross-server");			// Enable cross-server chat
	public static final ConfigurationHolder NEWSENABLED = new ConfigurationHolder(true, "module", "news");					// Enable server news
	public static final ConfigurationHolder POLLENABLED = new ConfigurationHolder(true, "module", "poll");					// Enable polls
	public static final ConfigurationHolder WIKIENABLED = new ConfigurationHolder(true, "module", "wiki");					// Enable in-game wiki
	public static final ConfigurationHolder VOICEENABLED = new ConfigurationHolder(true, "module", "voice");					// Enable in-game wiki


    // Chat
	public static final ConfigurationHolder BROADCAST = new ConfigurationHolder(true, "chat", "broadcast");						// Broadcast channel entry/leaves
	public static final ConfigurationHolder USERCHANNELS = new ConfigurationHolder(true, "chat", "userchannels");				// Users can create channels
	public static final ConfigurationHolder PRIVATEMESSAGES = new ConfigurationHolder(true, "chat", "privatemessages");			// Users can send PMs
	public static final ConfigurationHolder OFFLINEMESSAGES = new ConfigurationHolder(true, "chat", "offlinemessages");			// Users can receive offline messages
	public static final ConfigurationHolder DEFAULTCHANNEL = new ConfigurationHolder("general", "chat", "defaultchannel");   	// Default channel name
	public static final ConfigurationHolder CHANNELSFILE = new ConfigurationHolder("channels.yml", "chat", "channelsfile");		// Channels config file name
	public static final ConfigurationHolder LOCALCHAT = new ConfigurationHolder(true, "chat", "localchat");						// Local (say) chat enabled
	
	
	public PluginConfiguration(File dataFolder) {
        super(new YamlConfiguration(new File(dataFolder, "config.yml")));
    }
	
	@Override
	public void load() throws ConfigurationException {
		super.load();
		super.save();
	}

	@Override
	public void save() throws ConfigurationException {
		super.save();
	}

}
