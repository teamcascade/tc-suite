package org.teamcascade.tcsuite.configuration;

import java.io.File;

import org.spout.api.exception.ConfigurationException;
import org.spout.api.util.config.Configuration;
import org.spout.api.util.config.ConfigurationHolder;
import org.spout.api.util.config.ConfigurationHolderConfiguration;
import org.spout.api.util.config.yaml.YamlConfiguration;

public class ChannelConfiguration extends ConfigurationHolderConfiguration {

	// General chat channel (default join)
	public static final ConfigurationHolder GENERALTYPE = new ConfigurationHolder("local","general", "type");		
	public static final ConfigurationHolder GENERALRANGE = new ConfigurationHolder(0,"general", "range");			
	public static final ConfigurationHolder GENERALPASSWORD = new ConfigurationHolder("","general", "password");
	public static final ConfigurationHolder GENERALDESCRIPTION = new ConfigurationHolder("General chat channel","general", "description");

	// Admin chat channel 
	public static final ConfigurationHolder ADMINTYPE = new ConfigurationHolder("local","admin", "type");		
	public static final ConfigurationHolder ADMINRANGE = new ConfigurationHolder(0,"admin", "range");			
	public static final ConfigurationHolder ADMINPASSWORD = new ConfigurationHolder("","admin", "password");	
	public static final ConfigurationHolder ADMINDESCRIPTION = new ConfigurationHolder("Administrator chat channel","admin", "description");

	// System chat channel 
	public static final ConfigurationHolder SYSTEMTYPE = new ConfigurationHolder("local","system", "type");		
	public static final ConfigurationHolder SYSTEMRANGE = new ConfigurationHolder(0,"system", "range");			
	public static final ConfigurationHolder SYSTEMPASSWORD = new ConfigurationHolder("","system", "password");	
	public static final ConfigurationHolder SYSTEMDESCRIPTION = new ConfigurationHolder("Administrator chat channel","system", "description");

	public ChannelConfiguration(File dataFolder) {
        super(new YamlConfiguration(new File(dataFolder, "channels.yml")));
    }
	
	@Override
	public void load() throws ConfigurationException {
		super.load();
		super.save();
	}

	@Override
	public void save() throws ConfigurationException {
		super.save();
	}

}
