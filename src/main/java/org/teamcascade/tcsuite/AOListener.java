package org.teamcascade.tcsuite;



import org.spout.api.entity.Player;
import org.spout.api.event.EventHandler;
import org.spout.api.event.Listener;
import org.spout.api.event.Order;
import org.spout.api.event.player.PlayerLeaveEvent;
import org.spout.api.event.player.PlayerLoginEvent;
import org.teamcascade.tcsuite.user.User;


public class AOListener implements Listener {
	
	private final AOSuite plugin;
	
	public AOListener(AOSuite pPlugin) {
		this.plugin = pPlugin;
	}
	
	@EventHandler(order = Order.LATEST)
	public void onPlayerLogin(PlayerLoginEvent event) {
		if (!event.isAllowed()) {
			return;
		}
		Player player = event.getPlayer(); 
		String playerName = player.getName();
		
        // Load user saved preferences
		User user = new User(playerName);
		user.readData();
		// Add user to the plugin users via UserManager 
	   AOSuite.getUsersManager().addUser(user);
	
	}
	
	
	@EventHandler(order = Order.LATE)
	public void onPlayerLeave(PlayerLeaveEvent event) {
	
		String playerName = event.getPlayer().getName();
		User user = AOSuite.getUsersManager().getUser(playerName);
	
		user.writeData();

	}
	

}
