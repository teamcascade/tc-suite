/**
 * AOData represents the interface used to load/save data 
 */
package org.teamcascade.tcsuite.data;

import org.teamcascade.tcsuite.user.User;

public interface AOData {
	
	// User Data
	
	/**
	 * Gets the user data for a specific user
	 * @param pUserName
	 * @return User
	 */
	User getUser(String pUserName);
	
	/**
	 * Adds a user to the database
	 * @param pUser
	 * @return True if successful
	 */
	Boolean addUser(User pUser);
	
	
	
	// Channel Data
	
	
	
	

}
