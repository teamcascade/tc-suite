package org.teamcascade.tcsuite.data;


public class SQLDataFactory {
	
	public static SQLData getSQLDataHandler() {
		 // Currently only SQLLite is available for data handling
		 // Other data factory handlers can be easily added later on down the road
		return new SQLLiteData();
   }

}
