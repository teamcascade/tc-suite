package org.teamcascade.tcsuite.command;

import java.util.Collection;
import java.util.Iterator;


import org.spout.api.chat.ChatArguments;
import org.spout.api.command.CommandContext;
import org.spout.api.command.CommandSource;
import org.spout.api.command.annotated.Command;
import org.spout.api.exception.CommandException;
import org.teamcascade.tcsuite.AOSuite;

public class ChatCommands {
	private final AOSuite plugin;


	public ChatCommands(AOSuite pPlugin) {
		this.plugin = pPlugin;
	}
	
		
	/**
	 * Version command will let a user display the current version of the plugin
	 * @param args
	 * @param source
	 * @throws CommandException
	 */
	@Command(aliases = {"version"}, usage = "", desc = "Display the AO plugin version", min = 0, max = 0)
	public void version(CommandSource source, CommandContext args)  throws CommandException  {
		ChatArguments msg1 = new ChatArguments("AO Suite: " +AOSuite.getInstance().getDescription().getVersion());
		ChatArguments msg2 = new ChatArguments("See http://www.aocraft.net for latest releases");
//		AOSuite.getChatMessageManager().Send(source.getName(),"system",msg1, msg1);
//		AOSuite.getChatMessageManager().Send(source.getName(),"system",msg2, msg2);
		
	}
	
		
	/**
	 * Join command will let a user join a specific chat channel
	 * @param args
	 * @param source
	 * @throws CommandException
	 */
	@Command(aliases = {"join"}, usage = "<chat channel> <password>", desc = "Join a chat channel", min = 1, max = 2)
	public void join(CommandContext args, CommandSource source) throws CommandException {
	}
	
	
	/**
	 * Leave command will let a user leave a specific chat channel
	 * @param args
	 * @param source
	 * @throws CommandException
	 */
	@Command(aliases = {"leave"}, usage = "<chat channel>", desc = "Leave a chat channel", min = 1, max = 1)
	public void leave(CommandContext args, CommandSource source) throws CommandException {
			
	}


	/**
	 * Channels command will let a user list all channels on server
	 * @param args
	 * @param source
	 * @throws CommandException
	 */
	@Command(aliases = {"channels"}, usage = "[<username>]", desc = "List all chat channels or the ones a user is registered to", min = 0, max = 1)
	public void channels(CommandContext args, CommandSource source) throws CommandException {
	}
	/**
	 * Channels command will let a user list all channels on server
	 * @param args
	 * @param source
	 * @throws CommandException
	 */

	@SuppressWarnings("deprecation")
	@Command(aliases = {"who","list"}, usage = "[<username>]", desc = "Display the information about one or more server players", min = 0, max = 1)
	public void who(CommandContext args, CommandSource source) throws CommandException {

	}

}
