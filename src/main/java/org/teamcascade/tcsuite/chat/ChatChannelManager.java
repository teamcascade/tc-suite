package org.teamcascade.tcsuite.chat;

import java.util.HashMap;


public class ChatChannelManager {
	// Holds the currently created chat channels
	HashMap<String,ChatChannel> chatChannels ;
	
	public ChatChannelManager(){
		chatChannels = new HashMap<String,ChatChannel>();
	}
	
	/**
	 * 
	 * @param 	pChannel
	 * @return	True			Channel has been added 
	 * @return	False			Channel could not be added
	 */
	public void addChatChannel (ChatChannel pChannel) {
	      chatChannels.put(pChannel.getChName(),pChannel);
	}
	
	public void removeChatChannel(ChatChannel pChannel) {
		chatChannels.remove(pChannel.getChName());
	}
	/**
	 * Check if a channel with a specific name exists.
	 * Channel names have to be unique and this should be called before adding any channels as a precaution
	 * addChatChannel calls it as an integrity safeguard but it should be called before adding the channel
	 * 
	 * @param 	pChannelName	Channel name to check existence of
	 * @return	True			Channel exists already
	 * @return 	False			Channel does not exists			
	 */
	public boolean Exists(String pChannelName) {
		return chatChannels.containsKey(pChannelName);
	}
	
	/**
	 * Return a ChatChannel with a specified channel name
	 */
	public ChatChannel getChannel(String pChannelName) {
		return chatChannels.get(pChannelName);
	}
}
