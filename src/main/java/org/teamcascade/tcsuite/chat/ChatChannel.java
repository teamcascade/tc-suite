package org.teamcascade.tcsuite.chat;

import java.sql.Timestamp;
import java.util.ArrayList;


public class ChatChannel {
	// Define fields
																
	private String chType;											// Type of channel
	private String chName ;											// Unique name of channel
	private Integer chRange ;										// Range the channel can be heard, 0 for global
	private String chPass ;											// Password for channel or null
	private String chDescription ;									// Description of channel purpose
 
	
	// Constructors
	public ChatChannel(String pName, String pType) {
		this.chName = pName;
		this.chType = pType;
		this.chRange=0;
		this.chPass="";
		this.chDescription=pType.toString().trim() + pName + " chat channel";
	}
	public ChatChannel(String pName) {
		this.chName = pName;
		this.chType = "local";
		this.chRange=0;
		this.chPass="";
		this.chDescription=chType.toString().trim() + pName + " chat channel";
	}
	// Methods
	
	
	// Getters and Setters

	public String getChName() {
		return chName;
	}

	public void setChName(String chName) {
		this.chName = chName;
	}
	public String getChType() {
		return chType;
	}

	public void setChType(String chType) {
		this.chType = chType;
	}

	public Integer getChRange() {
		return chRange;
	}

	public void setChRange(Integer chRange) {
		this.chRange = chRange;
	}

	public String getChPass() {
		return chPass;
	}

	public void setChPass(String chPass) {
		this.chPass = chPass;
	}

	public String getChDescription() {
		return chDescription;
	}

	public void setChDescription(String chDescription) {
		this.chDescription = chDescription;
	}
	

}
