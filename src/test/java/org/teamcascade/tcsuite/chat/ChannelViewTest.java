package org.teamcascade.tcsuite.chat;

import static org.junit.Assert.*;


import org.junit.Test;
import org.teamcascade.tcsuite.chat.ChannelView;

public class ChannelViewTest {
	// Constructor
	ChannelView view1;
	ChannelView view2;
	
	public void setup() {
		view1 = new ChannelView("Verdana", "white", false, false);
		view2 = new ChannelView("Courier", "blue", true, true);
	}
	
	@Test
	public void testChannelView() {
		view1 = new ChannelView("Verdana", "white", false, false);
		assertNotNull(view1);									// Make sure the object is not null;
		assertEquals("Verdana", view1.getViewFontFamily());		// Font-family should be Verdana
		assertEquals("white", view1.getViewFontColor());		// Color should be white 
		assertFalse(view1.getViewFontBold());
		assertFalse(view1.getViewFontItalic());
		
	}

	@Test
	public void testGettersAndSetters(){
		setup();
		// initial values to check the Getters
		assertEquals("Verdana", view1.getViewFontFamily());		// Font-family should be Verdana
		assertEquals("white", view1.getViewFontColor());		// Color should be white 
		assertFalse(view1.getViewFontBold());
		assertFalse(view1.getViewFontItalic());
        // set new values using the Setters
		view1.setViewFontFamily("Courier");
		view1.setViewFontColor("blue");
		view1.setViewFontItalic(true);
		view1.setViewFontBold(true);
		// verify new values with the Getters 
		assertEquals("Courier", view1.getViewFontFamily());		// Font-family should be Courier
		assertEquals("blue", view1.getViewFontColor());		// Color should be blue
		assertTrue(view1.getViewFontBold());
		assertTrue(view1.getViewFontItalic());


	}
}
