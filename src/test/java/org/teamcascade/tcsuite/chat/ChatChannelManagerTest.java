package org.teamcascade.tcsuite.chat;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.teamcascade.tcsuite.chat.ChatChannel;
import org.teamcascade.tcsuite.chat.ChatChannelManager;

public class ChatChannelManagerTest {

	ChatChannelManager manager;
	ChatChannel ch1;
	ChatChannel ch2;
	
	@Before
	public void setUp() throws Exception {
		
		manager = new ChatChannelManager();
		ch1 = new ChatChannel("General","Local");
		ch2 = new ChatChannel("Admin","Local");
	}

	@Test
	public void testChatChannelManager()  {
		assertNotNull(manager);
		assertTrue(manager.chatChannels.size()==0);
		
		
	}
	
	@Test
	public void testAddChatChannel() {
		assertNotNull(manager);
		assertTrue(manager.chatChannels.size()==0);
		manager.addChatChannel(ch1);
		assertTrue(manager.chatChannels.size()==1);
		assertNotNull(manager.chatChannels.get("General"));
		assertEquals(ch1, manager.chatChannels.get("General"));
		
		
	}
	
	@Test
	public void testRemoveChatChannel() {
		assertNotNull(manager);
		assertTrue(manager.chatChannels.size()==0);
		manager.addChatChannel(ch1);
		assertTrue(manager.chatChannels.size()==1);
		manager.addChatChannel(ch2);
		assertTrue(manager.chatChannels.size()==2);
        manager.removeChatChannel(ch1);
		assertTrue(manager.chatChannels.size()==1);
		assertNull(manager.chatChannels.get("General"));
		assertNotNull(manager.chatChannels.get("Admin"));

        
	}

	@Test
	public void testGetChatChannel() {
		assertNotNull(manager);
		assertTrue(manager.chatChannels.size()==0);
		manager.addChatChannel(ch1);
		manager.addChatChannel(ch2);
		assertEquals(manager.getChannel("General"), ch1);
		assertEquals(manager.getChannel("Admin"), ch2);
		assertNull(manager.getChannel("System"));
		

	}

	@Test
	public void testExists() {
		assertNotNull(manager);
		assertTrue(manager.chatChannels.size()==0);
		manager.addChatChannel(ch1);
		assertFalse(manager.Exists("Admin"));
		assertTrue(manager.Exists("General"));
		manager.addChatChannel(ch2);
		assertTrue(manager.Exists("Admin"));
		manager.removeChatChannel(ch1);
		assertFalse(manager.Exists("General"));
		
		
		}


}
