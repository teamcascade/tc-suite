package org.teamcascade.tcsuite.chat;

import static org.junit.Assert.*;


import org.junit.Test;
import org.teamcascade.tcsuite.chat.ChatChannel;

public class ChatChannelTest {

	private ChatChannel ch1;
	private String channelName;
	private String username1;
	private String username2;
	
	private void setup() {
		// setup channel
		channelName="General";
		String chType = "local";
		ch1 = new ChatChannel(channelName, chType);
		username1 = "TestUser1";
		username2 = "TestUser2";
		ch1.setChDescription("Default chat channel");
		ch1.setChPass("test");
		ch1.setChRange(100);
	
	}

	@Test
	public void testChatChannel() {
		String chType = "local";
		ChatChannel ch1 = new ChatChannel(channelName, chType);
		assertEquals(channelName, ch1.getChName());

	}
	
	@Test
	public void testGetterSetters() {
		setup();
		// User initial values to check the Getters
		assertEquals("local",ch1.getChType());
		assertEquals("General",ch1.getChName());
		assertEquals("Default chat channel",ch1.getChDescription());
		assertEquals("test",ch1.getChPass());
		assertTrue(ch1.getChRange() == 100);		
		
		// Change values with the Setters
		ch1.setChName("admin");
		ch1.setChType("system");
		ch1.setChDescription("System chat channel");
		ch1.setChPass("test2");
		ch1.setChRange(0);
		
		// Verify changed values with the Getters

		assertEquals("system",ch1.getChType());
		assertEquals("admin",ch1.getChName());
		assertEquals("System chat channel",ch1.getChDescription());
		assertEquals("test2",ch1.getChPass());
		assertTrue(ch1.getChRange() == 0);		

	}
	

}
