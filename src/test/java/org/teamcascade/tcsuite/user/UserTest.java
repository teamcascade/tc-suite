package org.teamcascade.tcsuite.user;

import static org.junit.Assert.*;

import java.util.Calendar;
import java.util.Date;


import org.junit.Test;
import org.teamcascade.tcsuite.user.User;

public class UserTest {

	String username1;
	User user1;
	String username2;
	User user2;
	Calendar today ;
	Calendar yesterday ;
	private void setup() {
		username1 = "User Name 1";
		user1 = new User(username1);
		username2 = "User Name 2";
		user2 = new User(username2);
		today = Calendar.getInstance();			 // today
		yesterday = Calendar.getInstance(); 		
		yesterday.add(Calendar.DAY_OF_YEAR, -1); // yesterday
		user1.setUserLastOnline(today);
		user2.setUserLastOnline(yesterday);

	}
	
	@Test
	public void testUser() {
		username1 = "User Name 1";
		User user = new User(username1);
		assertNotNull("User cannot be null", user);
		
	}

	@Test
	public void testGetName() {
		setup();
		assertEquals("User1 name does not match", "User Name 1", user1.getUserName());
	}
	
	@Test
	public void testSetUserLastOnline() {
		setup();
		assertNotNull(user1.getUserLastOnline());
		assertNotNull(user2.getUserLastOnline());
		assertEquals(today, user1.getUserLastOnline());
		assertEquals(yesterday, user2.getUserLastOnline());
		user2.setUserLastOnline(today);
		assertEquals(user2.getUserLastOnline(), user2.getUserLastOnline());
		assertFalse(yesterday == user1.getUserLastOnline());
		user1.setUserLastOnline(yesterday);
		assertEquals(yesterday, user1.getUserLastOnline());
		
		
	}

	@Test
	public void testGetUserLastOnline() {
		setup();
		assertNotNull(user2.getUserLastOnline());
		assertEquals(today, user1.getUserLastOnline());
		user1.setUserLastOnline(yesterday);
		assertEquals(yesterday, user1.getUserLastOnline());
		
	}


	
}
